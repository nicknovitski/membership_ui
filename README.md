# DSA SF Membership Portal UI

A graphical user interface for the [DSA SF Membership API](https://gitlab.com/DSASanFrancisco/membership_api)

# Installation

* Install HomeBrew from https://brew.sh
* Download and install Node / NPM

Our recommendation is to install node through [nvm](https://github.com/creationix/nvm). The instructions are:
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install v6.14.2
nvm use v6.14.2
```

The sass library we use only works with node 6.x for macOS, so nvm makes it easy to use that specific version.

You can also use homebrew:

```
brew install node
```

* Create your `.env` config file

```
cp example.env .env
# edit .env to your liking
```

* Install dependencies

```
npm install
```

* Run the server

```
npm run dev
```
