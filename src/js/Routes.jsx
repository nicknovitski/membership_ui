import React, { Component } from 'react'
import { Router, Route, IndexRedirect } from 'react-router'
import { routerActions } from 'react-router-redux'
import { connect } from 'react-redux'
import { login, logout } from './redux/actions/authActions'

import App from './components/App'
import Meetings from './components/meeting/Meetings'
import MeetingKiosk from './components/meeting/MeetingKiosk'
import MeetingAdmin from './components/meeting/MeetingAdmin'
import MemberPage from './components/membership/MemberPage'
import Admin from './components/admin/Admin'
import Committees from './components/committee/Committees'
import Elections from './components/election/Elections'
import ElectionDetail from './components/election/ElectionDetail'
import ElectionEdit from './components/election/ElectionEdit'
import NotFound from './components/common/NotFound'
import EnterVote from './components/election/EnterVote'
import PrintBallots from './components/election/PrintBallots'
import SignInKiosk from './components/election/SignInKiosk'
import Vote from './components/election/Vote'
import EmailAdmin from './components/email/EmailAdmin'
import Resources from './components/resources/Resources'

import { USE_AUTH } from './config'
import { UserAuthWrapper } from 'redux-auth-wrapper'
import { RankedChoiceVisualization } from './components/election/RankedChoiceVisualization'

const UserIsAuthenticated = UserAuthWrapper({
  authSelector: state => state.auth.get('user'),
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserIsAuthenticated',
  predicate: user => (USE_AUTH ? user : true)
})

const Authenticated = UserIsAuthenticated(props => props.children)

class Routes extends Component {
  shouldComponentUpdate () {
    return false
  }

  render () {
    return (
      <Router history={this.props.history}>
        <Route path="/" component={App}>
          <IndexRedirect to="members" />
          <Route path="process" />
          <Route path="login" onEnter={() => this.props.login()} />
          <Route path="logout" onEnter={() => this.props.logout()} />

          <Route component={Authenticated}>
            <Route path="members" component={MemberPage} />
            <Route path="members/:memberId" component={MemberPage} />
            <Route path="meetings" component={Meetings} />
            <Route path="meetings/:meetingId/kiosk" component={MeetingKiosk} />
            <Route path="meetings/:meetingId/admin" component={MeetingAdmin} />
            <Route path="committees" component={Committees} />
            <Route path="admin" component={Admin} />
            <Route path="elections" component={Elections} />
            <Route path="elections/:electionId" component={ElectionDetail} />
            <Route path="elections/:electionId/edit" component={ElectionEdit} />
            <Route
              path="elections/:electionId/results"
              component={RankedChoiceVisualization}
            />
            <Route
              path="elections/:electionId/results/fullscreen"
              component={RankedChoiceVisualization}
            />
            <Route
              path="admin/elections/:electionId/vote"
              component={EnterVote}
            />
            <Route
              path="elections/:electionId/print"
              component={PrintBallots}
            />
            <Route
              path="elections/:electionId/signin"
              component={SignInKiosk}
            />
            <Route path="admin/elections/:electionId/vote" component={EnterVote} />
            <Route path="elections/:electionId/print" component={PrintBallots} />
            <Route path="elections/:electionId/signin" component={SignInKiosk} />
            <Route path="vote/:electionId" component={Vote} />
            <Route path="email" component={EmailAdmin} />
            <Route path="resources" component={Resources} />
          </Route>
          <Route path="*" component={NotFound} />
        </Route>
      </Router>
    )
  }
}

const mapStateToProps = state => state
const mapDispatchToProps = dispatch => ({
  login: () => dispatch(login()),
  logout: () => dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(Routes)
