import { HTTP_DELETE, HTTP_GET, HTTP_POST, HTTP_PUT, HTTP_PATCH, logError } from '../util/util'
import { fromJS, Iterable, Set } from 'immutable'
import _ from 'lodash'
import { MEMBERSHIP_API_URL, USE_AUTH } from '../config'
import request from 'superagent'
import { getAdminCredentials } from '../redux/actions/authActions' // eslint-disable-line no-unused-vars
import { AUTH_STORE } from '../redux/reducers/auth'
import { serviceLocator } from '../util/serviceLocator'

// TODO: Add equality for ApiRequest
// TODO: Add tests

/**
 * An immutable {@link ApiRequest} builder with default arguments.
 */
export class ApiClient {

  constructor (defaultArgs = {}) {
    this.defaultArgs = defaultArgs
  }

  url (path, overrides = {}) {
    const args = _.merge(this.defaultArgs, overrides)
    return new ApiRequest({path, ...args})
  }

}

// Add basic auth headers to API request based on redux store
function addAuthHeaders (apiRequest) {
  if (USE_AUTH) {
    let authStore = serviceLocator.store.getState()[AUTH_STORE]
    const token = authStore.get('token')
    if (token) {
      apiRequest.set('Authorization', `Bearer ${token}`)
    }
  }
  return apiRequest
}

export const Api = new ApiClient({baseUrl: MEMBERSHIP_API_URL})

/**
 * An immutable-like request object that captures all information before issuing the request for
 * logging / debugging / sharing / testing purposes.
 *
 * Usage:
 *
 * const client = new ApiClient({baseUrl: 'http://localhost:8080'})
 * const api = client.url('/my/route')
 * const r1 = api.get() // GET http://localhost:8080/my/route
 * r1.executeOr({}) // makes request, handles error by logging and returns a Promise containing {}
 * r1.execute() // issues request again, handles error by logging and returns a failed Promise
 * const r2 = api.post({name: 'example'}) // POST http://localhost:8080/my/route {"name":"example"}
 * ...
 */
export class ApiRequest {

  static validatePathOrLogError ({method, path}) {
    if (!path) {
      logError(`Invalid path: '${path}' (${method})`)
      return false
    }
    return true
  }

  static validateHttpMethodOrLogError ({method, path}) {
    switch (method.toUpperCase()) {
      case HTTP_GET:
      case HTTP_PUT:
      case HTTP_POST:
      case HTTP_PATCH:
      case HTTP_DELETE:
        return true
      default:
        logError(`Unrecognized http method: '${method}' (/${path})`)
        return false
    }
  }

  static validateResponseOrLogError ({method, path, error}) {
    const { status } = error
    if (status >= 200 && status < 300) {
      return true
    } else {
      if (!status) {
        logError(`ERROR ${method} ${path}`, error)
      } else if (status >= 400 && status < 500) {
        logError(`BAD REQUEST ${method} ${path}`, error)
      } else if (status >= 500) {
        logError(`SERVER ERROR ${method} ${path}`, error)
      } else {
        logError(`Unrecognized or unexpected status code: ${status}`, error)
      }
      return false
    }
  }

  static defaultRespErrorHandler ({method, path, error}) {
    return (
      ApiRequest.validatePathOrLogError({method, path}) &&
      ApiRequest.validateResponseOrLogError({method, path, error})
    )
  }

  static defaultRequestValidator ({method, path}) {
    return (
      ApiRequest.validateHttpMethodOrLogError({method, path})
    )
  }

  constructor ({
    baseUrl,
    method,
    path,
    params = {},
    body = {},
    respErrorHandler = ApiRequest.defaultRespErrorHandler,
    requestValidator = ApiRequest.defaultRequestValidator
  }) {
    if (!baseUrl) {
      throw new Error(`ApiRequest.baseUrl must not be empty string or null`)
    }
    if (!path || !path.startsWith('/')) {
      throw new Error(`ApiRequest.path '${path}' must start with a forward slash '/'`)
    }
    this.baseUrl = baseUrl
    this.method = method ? method.toUpperCase() : null
    this.path = path
    this.params = params
    this._body = body
    this.respErrorHandler = respErrorHandler
    this.requestValidator = requestValidator

    this.url = this.baseUrl + this.path
  }

  execute (context = {}) {
    if (this.method === null) {
      throw new Error('ApiClient.execute called before .get, .post, .put, or .delete')
    }
    const body = this.bodyJson()
    const props = {
      method: this.method,
      path: this.path,
      params: this.params,
      url: this.url,
      body: body,
      response: null
    }
    const invalidKeys = Set(Object.keys(props)).intersect(Object.keys(context))
    if (invalidKeys.length > 0) {
      throw new Error(
        `Cannot pass the following keys in the ApiRequest context: ${invalidKeys.toArray()}`
      )
    }
    const descriptor = _.assign(props, context)
    this.requestValidator(descriptor)
    let apiRequest
    switch (this.method) {
      case 'GET':
        apiRequest = request.get(this.url)
        break
      case 'DELETE':
        apiRequest = request.delete(this.url)
        break
      case 'PUT':
        apiRequest = request.put(this.url)
        break
      case 'POST':
        apiRequest = request.post(this.url)
        break
      case 'PATCH':
        apiRequest = request.patch(this.url)
        break
    }
    if (this.params) {
      apiRequest = apiRequest.query(this.params)
    }
    if (Object.keys(body).length > 0) {
      apiRequest = apiRequest.send(body)
    }
    return addAuthHeaders(apiRequest).then(result => {
      const immutableResult = result.body ? fromJS(result.body) : null
      return Promise.resolve(immutableResult)
    }).catch((err) => {
      descriptor.error = err
      this.respErrorHandler(descriptor)
      return Promise.reject(descriptor)
    })
  }

  executeOr (orElse, context = {}) {
    return this.execute(context).catch(() => Promise.resolve(orElse))
  }

  copy ({
    baseUrl = this.baseUrl,
    method = this.method,
    path = this.path,
    params = this.params,
    body = this._body,
    respErrorHandler = this.respErrorHandler,
    requestValidator = this.requestValidator
  }) {
    return new ApiRequest({baseUrl, method, path, params, body, respErrorHandler, requestValidator})
  }

  toString () {
    const header = `${this.method} ${this.baseUrl}${this.url}`
    const body = this.bodyJson()
    let prettyJsonBody
    if (body) {
      prettyJsonBody = JSON.stringify(body, null, 2)
    }
    switch (this.method) {
      case 'POST':
      case 'PUT':
        return `${header}\n\n${prettyJsonBody}`
      default:
        return header
    }
  }

  bodyJson () {
    if (!_.isObject(this._body)) return null
    else return Iterable.isIterable(this._body) ? this._body.toObject() : this._body
  }

  /**
   * Provide a function that is called after the request completes.
   *
   * The second argument is the default handler for this API so that you can call it to have the
   * default logging of errors or whatever is configured for this API.
   *
   * @param recoverFn ((Descriptor => *), Descriptor) => * a function that combines
   *                  the previous handler with the API result descriptor. You should call
   *                  the default handler if you want to chain effects, such as logging.
   *                  NOTE: Returning a Promise will not wait until the promise completes
   *                  before completing the Promise returned by execute()
   * @returns {*} Ignored outside of error handlers,
   *              but default handlers return boolean so you can && short-circuit.
   */
  composeErrorHandler (recoverFn) {
    const compositeErrorHandler = (descriptor) => recoverFn(this.respErrorHandler, descriptor)
    return this.copy({respErrorHandler: compositeErrorHandler})
  }

  query (params) {
    if (_.isPlainObject(params)) {
      return this.copy({params})
    } else {
      throw new Error(`Invalid query string params: ${params}`)
    }
  }

  get () {
    if (this.method === HTTP_GET) return this
    else return this.copy({method: HTTP_GET})
  }

  put (body) {
    return this.copy({method: HTTP_PUT, body})
  }

  post (body) {
    return this.copy({method: HTTP_POST, body})
  }

  patch (body) {
    return this.copy({method: HTTP_PATCH, body})
  }

  remove () {
    if (this.method === HTTP_DELETE) return this
    else return this.copy({method: HTTP_DELETE})
  }

}
