import {Api} from '../client/ApiClient'

export class CommitteeClient {

  constructor (api) {
    this.api = api
  }

  all () {
    return this.api.url(`/committee/list`).get().execute()
  }

}

export const Committees = new CommitteeClient(Api)
