import { Api } from '../client/ApiClient'

export class MeetingClient {

  constructor (api) {
    this.api = api
  }

  isValidMeetingCode (code) {
    let codeInt
    try {
      codeInt = parseInt(code, 10)
    } catch (e) {
      codeInt = 0
    }
    return codeInt >= 1000 && codeInt <= 9999
  }

  all () {
    return this.api.url(`/meeting/list`).get().execute()
  }

  get (meetingId) {
    return this.api.url(`/meetings/${meetingId}`).get().execute()
  }

  getAttendees (meetingId) {
    return this.api.url(`/meetings/${meetingId}/attendees`).get().execute()
  }

  addAttendee (meetingId, memberId) {
    return this.api.url(`/member/attendee`).post({meeting_id: meetingId, member_id: memberId}).execute()
  }

  removeAttendee (meetingId, memberId) {
    return this.api.url(`/meetings/${meetingId}/attendees/${memberId}`).remove().execute()
  }

  generateMeetingCode (meetingId) {
    if (!meetingId) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    return this.api.url(`/meeting`).patch({
      meeting_id: meetingId,
      code: 'autogenerate'
    }).execute()
  }

  setMeetingCode (meetingId, code) {
    if (!meetingId) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    let validated
    if (code === '') {
      validated = ''
    } else {
      if (!this.isValidMeetingCode(code)) {
        throw new Error(`Invalid meeting code: ${code}`)
      }
      validated = parseInt(code, 10)
    }
    return this.api.url(`/meeting`).patch({
      meeting_id: meetingId,
      code: validated
    }).execute()
  }

  setLandingUrl (meetingId, landingUrl) {
    if (!meetingId) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    return this.api.url(`/meeting`).patch({
      meeting_id: meetingId,
      landing_url: landingUrl
    }).execute()
  }

  setMeetingTimes (meetingId, startTime, endTime) {
    if (!meetingId) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    return this.api.url(`/meeting`).patch({
      meeting_id: meetingId,
      start_time: startTime,
      end_time: endTime
    }).execute()
  }

}

export const Meetings = new MeetingClient(Api)
