import {Api} from '../client/ApiClient'
import {Map} from 'immutable'

export class MemberClient {

  constructor (api) {
    this.api = api
  }

  getCurrentUser () {
    return this.api.url(`/member/details`).get().executeOr(Map())
  }

  all () {
    return this.api.url(`/member/list`).get().execute()
  }

  search (query, pageSize = 10) {
    return this.api.url(`/member/search`).query({query: query, page_size: pageSize}).get().execute()
  }
}

export const Members = new MemberClient(Api)
