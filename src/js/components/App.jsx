import React, { Component } from 'react'
import { Grid } from 'react-bootstrap'
import NotificationSystem from 'react-notification-system'
import { get as _get } from 'lodash'
import classNames from 'classnames'

import Navigation from './navigation/Navigation.jsx'
import { serviceLocator } from '../util/serviceLocator'
import { isFullscreen } from '../util/isFullscreen'

export default class App extends Component {
  componentDidMount () {
    serviceLocator.notificationSystem = this.refs.notificationSystem
  }

  render () {
    let navContainer = null
    const pathname = _get(this.props, ['location', 'pathname'], '')

    if (!pathname || !isFullscreen(pathname)) {
      navContainer = (
        <div className="nav-container">
          <Navigation />
        </div>
      )
    }

    return (
      <div
        className={classNames('app-container', {
          fullscreen: isFullscreen(pathname)
        })}
      >
        {navContainer}
        <div className="content-container">
          <Grid fluid className="content">
            {this.props.children}
          </Grid>
        </div>

        <NotificationSystem ref="notificationSystem" />
      </div>
    )
  }
}
