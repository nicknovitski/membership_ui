import React, { Component } from 'react'
import { membershipApi } from '../../services/membership'
import { HTTP_GET, logError } from '../../util/util'
import { fromJS } from 'immutable'
import MemberList from '../admin/MemberList'

export default class Admin extends Component {
  constructor (props) {
    super(props)
    this.state = {
      admin: null
    }
  }

  componentDidMount () {
    this.fetchMemberData()
  }

  componentDidUpdate (prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.fetchMemberData()
    }
  }

  render () {
    if (!this.isAdmin()) {
      return <div></div>
    }

    return (
      <div>
        <MemberList admin={this.isAdmin()}/>
      </div>
    )
  }

  isAdmin () {
    if (this.state.admin === null) {
      return false
    }

    const roles = this.state.admin.getIn(['roles'], null)
    let admin = false
    if (roles !== null) {
      roles.forEach((role) => {
        if (role.get('role') === 'admin' && role.get('committee') === 'general') {
          admin = true
        }
      })
    }
    return admin
  }

  async fetchMemberData () {
    try {
      const results = await membershipApi(HTTP_GET, `/member/details`)
      this.setState({
        admin: fromJS(results)
      })
    } catch (err) {
      return logError('Error loading member details', err)
    }
  }
}
