import React, {Component} from 'react'
import FieldGroup from '../common/FieldGroup'
import {Api} from '../../client/ApiClient'
import {Members} from '../../client/MemberClient'
import {membershipApi} from '../../services/membership'
import {HTTP_GET, logError} from '../../util/util'
import {AttendeeEligibilityRow, EmptyEligibilityRow} from './MemberRow'
import {Button, Col, Form, Row} from 'react-bootstrap'
import {Link} from 'react-router'
import {List} from 'immutable'

export default class MemberList extends Component {

  constructor (props) {
    super(props)
    this.state = {
      committees: [],
      members: List(),
      newMember: {first_name: '', last_name: '', email_address: ''},
      admin: {email_address: '', committee: ''},
      inSubmission: false
    }
  }

  componentDidMount () {
    this.refreshMemberList()
    this.getCommittees()
  }

  refreshMemberList () {
    return Members.all().then((data) => {
      this.setState({members: data})
    })
  }

  updateForm (name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    let update = this.state[name]
    update[formKey] = value
    this.setState({[name]: update})
  }

  render () {
    if (!this.props.admin) {
      return <div></div>
    }
    const members = this.state.members.map((member, index) => {
      let status
      if (member.has('eligibility')) {
        status = <AttendeeEligibilityRow
          isEligible={member.getIn(['eligibility', 'is_eligible'])}
          message={member.getIn(['eligibility', 'message'])}
        />
      } else {
        status = <EmptyEligibilityRow />
      }
      return (
        <div key={`member-${index}`}>
          <Link to={`/members/${member.get('id')}`}>
            {`${member.get('name')}: ${member.get('email')}`}
          </Link>
          {status}
        </div>
      )
    })
    return (
      <div>
        <Row>
          <Col sm={4}>
            <h2>Add Member</h2>
            <Form horizontal onSubmit={(e) => e.preventDefault()}>
              <FieldGroup
                formKey="first_name"
                type="text"
                label="First Name"
                value={this.state.newMember.first_name}
                onFormValueChange={(formKey, value) => this.updateForm('newMember', formKey, value)}
                required
              />
              <FieldGroup
                formKey="last_name"
                type="text"
                label="Last Name"
                value={this.state.newMember.last_name}
                onFormValueChange={(formKey, value) => this.updateForm('newMember', formKey, value)}
                required
              />
              <FieldGroup
                formKey="email_address"
                type="text"
                label="Email"
                value={this.state.newMember.email_address}
                onFormValueChange={(formKey, value) => this.updateForm('newMember', formKey, value)}
                required
              />
              <Button type="submit" onClick={(e) => this.submitForm(e, 'newMember', '/member')}>Add Member</Button>
            </Form>
          </Col>
        </Row>
        <h2> Membership List </h2>
        {members}
      </div>
    )
  }

  async getCommittees () {
    try {
      const results = await membershipApi(HTTP_GET, `/committee/list`)
      this.setState({committees: results})
    } catch (err) {
      return logError('Error loading committee list', err)
    }
  }

  async submitForm (e, name, endpoint) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({inSubmission: true})
    try {
      await Api.url(endpoint)
        .post(this.state[name])
        .execute()
        .then(() => this.refreshMemberList())
    } catch (err) {
      return logError('Error submitting form', err)
    } finally {
      this.setState({inSubmission: false})
    }
  }
}
