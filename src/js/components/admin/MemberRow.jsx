import React, { Component } from 'react'

export class EmptyEligibilityRow extends Component {
  render () {
    return null
  }
}

export class AttendeeEligibilityRow extends Component {

  setStyle (isEligible) {
    const styles = {
      marginLeft: '10px'
    }

    if (!isEligible) {
      styles.color = 'red'
    }

    return styles
  }

  render () {
    const {
      isEligible,
      message
    } = this.props
    return (
      <span style={this.setStyle(isEligible)}>
        {message}
      </span>
    )
  }
}
