import React, { Component } from 'react'
import { connect } from 'react-redux'
import FieldGroup from '../common/FieldGroup'
import {
  Button,
  Col,
  Form
} from 'react-bootstrap'
import { List } from 'immutable'
import { addCommittee, fetchCommittees } from '../../redux/actions/committeeActions'

class ConnectedCommittees extends Component {
  constructor () {
    super()

    this.state = {
      name: '',
      adminEmails: '' // comma-separated list
    }

    this.admin = true // TODO: use a real one
    this.handleSubmit = this.handleSubmit.bind(this)
    this.updateName = this.updateName.bind(this)
    this.updateAdminEmails = this.updateAdminEmails.bind(this)
  }

  componentDidMount () {
    this.props.fetchCommittees(this.props.currentMember)
  }

  handleSubmit (event) {
    event.preventDefault()
    this.props.addCommittee({
      adminEmails: List(this.state.adminEmails.split(',').map(e => e.trim())),
      name: this.state.name,
      member: this.props.currentMember
    })
  }

  updateName (name) {
    this.setState({
      name: name
    })
  }

  updateAdminEmails (emails) {
    this.setState({
      adminEmails: emails
    })
  }

  render () {
    return (
      <div className="content container-fluid">
        <h2>Committees and Working Groups</h2>
        <ul>
          {this.props.committees.entrySeq().map(el => (
            <li key={el[0]}>
              {el[1]}
            </li>
          ))}
        </ul>

        { this.admin &&
        <div>
          <Col sm={4}>
            <h2>Add Committee</h2>
            <Form horizontal onSubmit={this.handleSubmit}>
              <FieldGroup
                formKey="name"
                type="text"
                label="Committee Name"
                value={this.state.name}
                onFormValueChange={(formKey, value) => this.updateName(value)}
                required
              />
              <FieldGroup
                formKey="emails"
                type="text"
                label="Admin Emails (comma separated)"
                value={this.state.adminEmails}
                onFormValueChange={(formKey, value) => this.updateAdminEmails(value)}
                required
              />

              <Button type="submit" className="btn btn-default">
                Add Committee
              </Button>
            </Form>
          </Col>
        </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  committees: state.committee.get('committees'),
  currentMember: state.committee.get('currentMember')
})

const mapDispatchToProps = (dispatch) => ({
  addCommittee: committee => dispatch(addCommittee(committee)),
  fetchCommittees: member => dispatch(fetchCommittees(member))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedCommittees)
