import React, { Component } from 'react'

export default class Candidate extends Component {
  render () {
    const imageUrl = this.props.imageUrl === null
      ? '../../../static/no_image.png'
      : this.props.imageUrl
    return (
      <div>
        <img src={imageUrl} style={{
          width: 'auto', height: this.props.height, marginRight: 30
        }}/>
        <span style={{fontSize: 36}}>
          {this.props.name}
        </span>
      </div>
    )
  }
}
