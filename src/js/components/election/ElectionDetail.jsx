import React, {Component} from 'react'
import {connect} from 'react-redux'
import {logError} from '../../util/util'
import {List, Map} from 'immutable'
import Candidate from './Candidate'
import {Elections} from '../../client/ElectionClient'
import {Members} from '../../client/MemberClient'
import {eligibleVoteStatus, electionStatus} from '../../services/elections'

class ElectionDetail extends Component {

  constructor (props) {
    super(props)
    this.state = {
      member: Map(),
      election: Map({
        name: '...',
        status: '...',
        candidates: List(),
        number_winners: '...',
        transitions: List()
      }),
      eligible: List(),
      inSubmission: false,
      results: Map()
    }
  }

  componentDidMount () {
    this.getMemberDetails()
    this.getElectionDetails()
    this.getEligibleVoters()
  }

  updateForm (name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    let update = this.state[name]
    update[formKey] = value
    this.setState({[name]: update})
  }

  render () {
    const {electionId} = this.props.params
    const candidates = this.state.election.get('candidates').map(candidate =>
      <Candidate key={candidate.get('id')} imageUrl={candidate.get('image_url')} name={candidate.get('name')} height={60}/>
    )

    const vote = this.props.member.getIn(['user', 'data', 'votes'], List())
      .find(vote => vote.get('election_id', '').toString() === electionId.toString())
    const voteStatus = eligibleVoteStatus(this.state.election.set('id', electionId), vote)
    // For some reason the election doesn't come with an election.id, but we need it
    const status = electionStatus(this.state.election)

    return (
      <div>
        <h2>View Election</h2>
        <h3>{this.state.election.get('name')}</h3>
        <p><b>Election status:</b> {status}</p>
        <p><b>Voter Status:</b> {voteStatus} <b>{this.renderVoteButton(electionId, voteStatus, status)}</b></p>
        <p><b>Eligible Voters:</b> {this.state.eligible.size}</p>
        <p><b>Number of positions:</b> {this.state.election.get('number_winners')}</p>
        <h2>Candidates</h2>
        { candidates }
      </div>
    )
  }

  renderVoteButton (electionId, voteStatus, electionStatus) {
    if (voteStatus === 'eligible' && electionStatus === 'polls open') {
      return <a href={`/vote/${electionId}`}>Vote Now!</a>
    } else {
      return null
    }
  }

  async getElectionDetails () {
    try {
      const results = await Elections.getElection(this.props.params.electionId)
      this.setState({election: results})
    } catch (err) {
      return logError('Error fetching election', err)
    }
  }

  async getEligibleVoters () {
    try {
      const results = await Elections.getEligibleVoters(this.props.params.electionId)
      this.setState({eligible: results || List()})
    } catch (err) {
      return logError('Error fetching eligible voters', err)
    }
  }

  // TODO: Move this to redux
  async getMemberDetails () {
    try {
      const member = await Members.getCurrentUser()
      this.setState({member})
    } catch (err) {
      return logError('Error loading member details', err)
    }
  }
}

export default connect((state) => state)(ElectionDetail)
