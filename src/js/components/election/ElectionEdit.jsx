import React, {Component} from 'react'
import {connect} from 'react-redux'
import members from '../../services/members'
import {Elections} from '../../client/ElectionClient'
import {logError} from '../../util/util'
import {Button} from 'react-bootstrap'
import {fromJS, List, Map} from 'immutable'
import Candidate from './Candidate'

class ElectionEdit extends Component {

  constructor (props) {
    super(props)
    this.state = {
      election: Map({name: '', candidates: List(), number_winners: 1, transitions: List()}),
      inSubmission: false,
      results: Map()
    }
  }

  componentDidMount () {
    this.getElectionDetails()
  }

  updateForm (name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    let update = this.state[name]
    update[formKey] = value
    this.setState({[name]: update})
  }

  render () {
    const admin = members.isAdmin(this.props.member)
    const candidates = this.state.election.get('candidates').map(candidate =>
      <Candidate key={candidate.get('id')} imageUrl={candidate.get('image_url')} name={candidate.get('name')} height={60}/>
    )
    const winners = this.state.results.get('winners', List()).map((winner, index) => {
      const candidateID = winner.get('candidate').toString()
      const candidateDetails = this.state.results.get('candidates').get(candidateID)
      return <div key={index}>{candidateDetails.get('name')}</div>
    })

    const transitions = this.state.election.get('transitions', List()).map(t =>
      <li key={t} style={{
        display: 'inline-block',
        marginRight: 10
      }}>
        <Button type="submit" value={t} onClick={(e) => this.submitTransition(e)}>{t}</Button>
      </li>
    )

    return admin
      ? (
        <div>
          <h2>Edit Election</h2>
          <h3>
            {this.state.election.get('name')} ({this.state.election.get('status')})
          </h3>
          <ul style={{
            listStyleType: 'none',
            paddingLeft: 0
          }}>
            { transitions }
          </ul>

          <h3>Number of positions {this.state.election.get('number_winners')} </h3>
          { candidates }
          <div>
            <Button type="submit" onClick={(e) => this.countVotes(e)}>Count The Vote</Button>
          </div>
          { (this.state.results.size > 0) &&
            <div>
              <h3>Winners</h3>
              { winners }
              <h3>Counts</h3>
              <div>{this.state.results.get('ballot_count')} ballots cast</div>
            </div>
          }
        </div>
      )
      : <div></div>
  }

  async getElectionDetails () {
    try {
      const results = await Elections.getElection(this.props.params.electionId)
      this.setState({election: fromJS(results)})
    } catch (err) {
      return logError('Error fetching election', err)
    }
  }

  async countVotes (e) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({inSubmission: true})
    try {
      const results = await Elections.countResults(this.props.params.electionId)
      this.setState({results: fromJS(results)})
    } catch (err) {
      return logError('Error counting votes', err)
    } finally {
      this.setState({inSubmission: false})
    }
  }

  async submitTransition (e) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    const transition = e.target.value
    alert(`Are you sure you want to ${transition} the election?`)
    this.setState({inSubmission: true})
    try {
      const results = await Elections.submitTransition(this.props.params.electionId, transition)
      const newElection = this.state.election
        .set('status', results.get('election_status'))
        .set('transitions', results.get('transitions'))

      this.setState({election: newElection})
    } catch (err) {
      return logError('Error changing election state', err)
    } finally {
      this.setState({inSubmission: false})
    }
  }
}

export default connect((state) => state)(ElectionEdit)
