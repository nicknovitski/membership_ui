import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BarChart, Bar, XAxis, YAxis, LabelList, ReferenceLine } from 'recharts'
import { membershipApi } from '../../services/membership'
import {
  extend as _extend,
  forEach as _forEach,
  get as _get,
  isObject as _isObject,
  map as _map,
  merge as _merge,
  orderBy as _orderBy,
  range as _range,
  set as _set
} from 'lodash'
import { HTTP_GET, logError } from '../../util/util'
import '../../util/rounding'
import { isFullscreen } from '../../util/isFullscreen'
import classNames from 'classnames'
import { arrayOf } from '../../util/getOfType'

export class RankedChoiceVisualization extends Component {
  constructor (props) {
    super(props)

    let colors = {
      totalVotes: '#ec1e24',
      excessVotes: '#ccc',
      eliminationVotes: '#666',
      blank: '#fff',
      quotaStroke: 'rgba(0,0,0,.25)',
      totalVotesText: '#fff'
    }

    let settings = {
      barSize: 20,
      width: 800,
      yAxisWidth: 200,
      height: 400,
      refLineSize: '5 3',
      refLineWidth: 1,
      refLineOffset: 10
    }

    const fullscreenLarge = null

    const fullscreen = isFullscreen(
      _get(this.props, ['location', 'pathname'], '')
    )

    if (fullscreen) {
      colors = _merge({}, colors, {
        blank: '#ec1e24',
        excessVotes: '#666',
        eliminationVotes: '#333',
        totalVotes: '#fff',
        totalVotesText: '#ec1e24'
      })

      settings = _merge({}, settings, {
        barSize: 24,
        width: 720,
        yAxisWidth: 210,
        height: 500,
        refLineSize: '5 3',
        refLineWidth: 4,
        refLineOffset: 10
      })
    }

    if (fullscreenLarge) {
      colors = _merge({}, colors, {
        blank: '#ec1e24',
        excessVotes: '#666',
        eliminationVotes: '#333',
        totalVotes: '#fff',
        totalVotesText: '#ec1e24'
      })

      settings = _merge({}, settings, {
        barSize: 30,
        width: 1000,
        yAxisWidth: 240,
        height: 600,
        refLineSize: '8 5',
        refLineWidth: 5,
        refLineOffset: 20
      })
    }

    const electionId = _get(this.props, ['params', 'electionId'])

    if (electionId) {
      this.constants = { electionId: parseInt(electionId, 10) }
    }

    this.settings = _merge({}, settings, {
      fullscreen,
      margins: { top: 0, right: 20, bottom: 0, left: 0 },
      title: 'SC Election 2018',
      colors,
      roundDesc: [
        'Starting Total Votes',
        {
          excess: 'Transfer of Excess Votes',
          elimination: 'Transfer of Eliminated Candidate Votes'
        },
        'Ending Vote Total'
      ],
      roundDivisor: 3,
      totalWinners: 5
    })

    this.state = {
      currentRound: null,
      subRound: null
    }

    // the .bind() calls are necessary due to the need for Stage 2 ES6
    // transpiling to support arrow functions for class methods.
    // more here: http://egorsmirnov.me/2015/08/16/react-and-es6-part3.html
    this.incrementRound = this.incrementRound.bind(this)
    this.decrementRound = this.decrementRound.bind(this)
    this.getOrderedData = this.getOrderedData.bind(this)
    this.getRoundDesc = this.getRoundDesc.bind(this)
    this.getDisplayWinners = this.getDisplayWinners.bind(this)
    this.getChart = this.getChart.bind(this)
  }

  async componentDidMount () {
    const { electionId } = this.constants
    let electionResults = null

    if (electionId) {
      electionResults = await this.getVotes(electionId)
    }

    if (electionResults) {
      this.constants = _merge({}, this.constants, electionResults)

      const { round_information: roundInformation } = this.constants

      if (roundInformation) {
        let roundVotes = []
        _forEach(roundInformation, (round, key) => {
          roundVotes.push(_get(round, 'votes'))
        })

        const maxVotesRound = _orderBy(
          roundVotes,
          [o => o.starting_votes + o.votes_received],
          ['desc']
        )[0]
        const maxVotesRecord = _orderBy(
          maxVotesRound,
          [o => o.starting_votes + o.votes_received],
          ['desc']
        )[0]
        const maxVotes =
          Math.round10(maxVotesRecord.starting_votes) +
          Math.round10(maxVotesRecord.votes_received)

        this.constants = _merge({}, this.constants, {
          maxVotes
        })
        this.setState({
          currentRound: 0,
          subRound: 1
        })
      }
    }
  }

  async getVotes (electionId) {
    try {
      return await membershipApi(HTTP_GET, '/election/count', {
        id: electionId,
        visualization: 1
      })
    } catch (err) {
      return logError('Error counting votes', err)
    }
  }

  incrementRound () {
    const { currentRound, subRound } = this.state
    const { roundDivisor } = this.settings

    let nextState = {}

    if (subRound % roundDivisor === 0) {
      nextState = {
        subRound: 1,
        currentRound: currentRound + 1
      }
    } else {
      nextState = {
        subRound: subRound + 1
      }
    }
    this.setState(_merge({}, this.state, nextState))
  }

  decrementRound () {
    const { currentRound, subRound } = this.state
    const { roundDivisor } = this.settings

    let nextState = {}

    if (subRound > 1) {
      nextState = {
        subRound: subRound - 1
      }
    } else {
      nextState = {
        currentRound: currentRound - 1,
        subRound: roundDivisor
      }
    }

    this.setState(_merge({}, this.state, nextState))
  }

  getOrderedData (data) {
    const { candidates } = this.constants
    _map(data, (datum, key) => {
      _set(datum, 'candidateId', parseInt(key))
      _set(datum, 'candidate', _get(candidates, [parseInt(key), 'name']))
    })
    return _orderBy(data, [o => o.starting_votes], ['desc'])
  }

  getRoundDesc () {
    const { currentRound, subRound } = this.state
    const { roundDesc } = this.settings
    const { round_information: roundInformation } = this.constants
    const transferType = _get(roundInformation, [currentRound, 'transfer_type'])

    const returnVal = roundDesc[subRound - 1]

    if (!_isObject(returnVal)) {
      return returnVal
    }

    return _get(roundDesc, [subRound - 1, transferType], '')
  }

  getChart () {
    const {
      round_information: roundInformation,
      maxVotes,
      quota
    } = this.constants
    if (roundInformation.length > 0) {
      const { currentRound, subRound } = this.state
      const transferType = _get(roundInformation, [
        currentRound,
        'transfer_type'
      ])
      const {
        barSize,
        refLineOffset,
        refLineSize,
        refLineWidth,
        roundDivisor,
        fullscreen,
        margins,
        width,
        height,
        yAxisWidth,
        colors
      } = this.settings
      let data = this.getOrderedData(
        _get(roundInformation, [currentRound, 'votes'])
      )
      data = data.reduce((newData, datum) => {
        const starting_votes = Math.round10(datum.starting_votes) // eslint-disable-line camelcase
        const votes_received = Math.round10(datum.votes_received) // eslint-disable-line camelcase
        const sum = starting_votes + votes_received // eslint-disable-line camelcase
        newData.push(
          _extend({}, datum, {
            starting_votes,
            votes_received,
            sum
          })
        )
        return newData
      }, [])

      const labelFormatter = value => {
        if (value > 0) {
          return value
        }
        return null
      }

      let transferColor = colors.blank
      let transferLabelList = null
      let totalLabelList = null
      const transferClass = 'onGrey'
      let totalClass = 'onRed'

      if (fullscreen) {
        totalClass = 'onWhite'
      }
      const containerClasses = classNames('chart', {
        fullscreen
      })

      if (subRound % roundDivisor === 0) {
        transferColor = colors.totalVotes
        transferLabelList = (
          <LabelList
            dataKey="sum"
            className={totalClass}
            position="insideRight"
          />
        )
      } else if (subRound === 2) {
        transferColor = _get(colors, `${transferType}Votes`)
        transferLabelList = (
          <LabelList
            formatter={labelFormatter}
            dataKey="votes_received"
            position="insideRight"
            className={transferClass}
          />
        )
        totalLabelList = (
          <LabelList
            dataKey="starting_votes"
            className={totalClass}
            position="insideRight"
          />
        )
      } else {
        totalLabelList = (
          <LabelList
            dataKey="starting_votes"
            className={totalClass}
            position="insideRight"
          />
        )
      }

      const ticks = _range(0, maxVotes, 5)

      return (
        <div className={containerClasses}>
          <BarChart
            layout="vertical"
            width={width}
            height={height}
            data={data}
            margin={margins}
          >
            <XAxis type="number" axisLine={false} ticks={ticks} />
            <YAxis
              dataKey="candidate"
              width={yAxisWidth}
              axisLine={false}
              type="category"
            />
            <Bar
              dataKey="starting_votes"
              stackId="a"
              barSize={barSize}
              fill={colors.totalVotes}
            >
              {totalLabelList}
            </Bar>
            <Bar
              dataKey="votes_received"
              stackId="a"
              barSize={barSize}
              fill={transferColor}
            >
              {transferLabelList}
            </Bar>
            <ReferenceLine
              x={quota}
              label={{
                value: `Threshold to win: ${quota}  votes`,
                angle: -90,
                position: 'left',
                offset: refLineOffset
              }}
              stroke={colors.quotaStroke}
              strokeDasharray={refLineSize}
              strokeWidth={refLineWidth}
            />
          </BarChart>
        </div>
      )
    }

    return null
  }

  getDisplayWinners () {
    const { winners, candidates } = this.constants
    const { currentRound } = this.state

    if (winners) {
      return (
        <ol>
          {winners.map((winner, id) => (
            <li key={`winner_${id}`}>
              {_get(winner, 'round') <= currentRound + 1
                ? _get(candidates, [_get(winner, 'candidate'), 'name'])
                : ''}
            </li>
          ))}
        </ol>
      )
    }
    return null
  }

  render () {
    let chart = null
    const { currentRound, subRound } = this.state
    const { round_information: roundInformation } = this.constants
    if (roundInformation) {
      chart = this.getChart()
    }
    const displayRound = currentRound + 1

    const displayWinners = this.getDisplayWinners()

    let previous = <button disabled>&laquo;</button>

    if (currentRound > 0 || subRound > 1) {
      previous = <button onClick={this.decrementRound}>&laquo;</button>
    }

    let next = <button disabled>&raquo;</button>

    if (currentRound < arrayOf(roundInformation).length - 1) {
      next = <button onClick={this.incrementRound}>&raquo;</button>
    }

    return (
      <div>
        <div className="titleBox">
          <h2 className="roundTitle">
            Round {displayRound} <small>{this.getRoundDesc()}</small>
          </h2>
          <h3 className="title rose">{this.settings.title}</h3>
        </div>
        <div className="chartBox">
          {chart}
          <div className="infoBox">
            <ul className="legend">
              <li>
                <i
                  style={{
                    backgroundColor: this.settings.colors.totalVotes
                  }}
                />
                <strong>Total Votes</strong>
              </li>
              <li>
                <i
                  style={{
                    backgroundColor: this.settings.colors.excessVotes
                  }}
                />
                <strong>Excess Votes</strong>
              </li>
              <li>
                <i
                  style={{
                    backgroundColor: this.settings.colors.eliminationVotes
                  }}
                />
                <strong>Elimination Votes</strong>
              </li>
            </ul>
            <div className="winnerBox">
              <h4>Winners</h4>
              {displayWinners}
            </div>
          </div>
        </div>
        <div className="buttonBox">
          {previous}
          {next}
        </div>
      </div>
    )
  }
}

export default connect(state => state)(RankedChoiceVisualization)
