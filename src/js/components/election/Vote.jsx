import React, { Component } from 'react'
import { connect } from 'react-redux'
import { membershipApi } from '../../services/membership'
import { HTTP_GET, HTTP_POST, logError } from '../../util/util'
import { Button, ButtonGroup, Form } from 'react-bootstrap'
import _ from 'lodash'
import { fromJS, is, Map, List } from 'immutable'

class Vote extends Component {

  constructor (props) {
    super(props)
    this.state = {
      election: new Map({name: '', number_winners: 1}),
      unranked: new List(),
      ranked: new List(),
      highlighted: null,
      inSubmission: false,
      result: null
    }
  }

  componentDidMount () {
    this.getElectionDetails()
  }

  updateForm (name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    let update = this.state[name]
    update[formKey] = value
    this.setState({[name]: update})
  }

  insertAtAnchor ({anchorIdx, candidate, ranked = this.state.ranked}) {
    const updatedRankings = ranked.insert(anchorIdx, candidate)
    this.setState({
      ranked: updatedRankings,
      highlighted: null // undo highlighting since either anchor or candidate was highlighted
    })
  }

  selectRanked (selected) {
    const highlighted = this.state.highlighted
    if (selected && highlighted) {
      const ranked = this.state.ranked
      const reinsertAtAnchor = ({anchorIdx, candidateIdx}) => {
        // skip anchors that are above or below the candidate, since they won't move
        if (candidateIdx === anchorIdx || candidateIdx === anchorIdx - 1) {
          this.setState({highlighted: null})
        } else {
          this.insertAtAnchor({
            // move the anchor up one if it comes after the candidate being moved
            anchorIdx: anchorIdx > candidateIdx ? anchorIdx - 1 : anchorIdx,
            candidate: ranked.get(candidateIdx),
            ranked: ranked.remove(candidateIdx)
          })
        }
      }
      switch (highlighted.get('type')) {
        case 'anchor':
          switch (selected.get('type')) {
            case 'anchor':
              // swap highlighted anchors or toggle anchor highlighting if clicked twice
              this.setState({highlighted: is(highlighted, selected) ? null : selected})
              break
            case 'candidate':
              // reinsert ranked candidate at highlighted anchor
              reinsertAtAnchor({
                anchorIdx: highlighted.get('idx'),
                candidateIdx: selected.get('idx')
              })
              break
          }
          break
        case 'candidate':
          const highlightedCandidateIdx = highlighted.get('idx')
          const highlightedCandidate = ranked.get(highlightedCandidateIdx)
          switch (selected.get('type')) {
            case 'anchor':
              // reinsert highlighted ranked candidate at anchor
              reinsertAtAnchor({
                anchorIdx: selected.get('idx'),
                candidateIdx: highlightedCandidateIdx
              })
              break
            case 'candidate':
              // swap both ranked candidates
              const candidateArray = ranked.toArray()
              const secondCandidateIdx = selected.get('idx')
              const candidateClicked = candidateArray[secondCandidateIdx]
              candidateArray[highlightedCandidateIdx] = candidateClicked
              candidateArray[secondCandidateIdx] = highlightedCandidate
              const updatedRankings = new List(candidateArray)
              this.setState({
                ranked: updatedRankings,
                highlighted: null
              })
              break
          }
          break
      }
    } else if (selected) {
      this.setState({highlighted: selected})
    }
  }

  addToRanked (unrankedIdx) {
    let anchorIdx = null
    if (this.state.highlighted) {
      const { type, idx } = this.state.highlighted.toObject()
      if (type === 'anchor') {
        anchorIdx = idx
      }
    }
    const candidate = this.state.unranked.get(unrankedIdx)
    if (anchorIdx !== null) {
      this.insertAtAnchor({anchorIdx, candidate})
    } else {
      this.setState({
        ranked: this.state.ranked.push(candidate)
      })
    }
    this.setState({
      unranked: this.state.unranked.remove(unrankedIdx)
    })
  }

  removeFromRanked (rankedIdx) {
    const candidate = this.state.ranked.get(rankedIdx)
    this.setState({
      unranked: this.state.unranked.push(candidate),
      ranked: this.state.ranked.remove(rankedIdx)
    })
  }

  render () {
    return this.renderForMobile()
  }

  renderForMobile () {
    const highlighted = this.state.highlighted || new Map()
    const ranked = this.state.ranked
    const rankedCandidates = ranked.map((candidate, idx) => {
      const current = new Map({type: 'candidate', idx})
      return <div key={`ranked-${idx}`}>
        <div style={{display: 'flex'}}>
          {Vote.candidateImage(candidate)}
          <ButtonGroup justified bsSize="large">
            <a
              id={`swap-candidate-${candidate.get('id')}`}
              className={`btn btn-success ${
                is(current, highlighted) ? 'active hover' : ''
              }`}
              onClick={(e) => {
                this.selectRanked(current)
              }}
            >{candidate.get('name')}</a>
            <a
              className='btn btn-danger'
              id={`drop-candidate-${candidate.get('id')}`}
              onClick={(e) => {
                this.removeFromRanked(idx)
              }}
            >Unrank</a>
          </ButtonGroup>
        </div>
      </div>
    })
    const anchor = (idx) => {
      const current = new Map({type: 'anchor', idx})
      return <div
        key={`anchor-${idx}`}
        className={`anchor ${is(current, highlighted) ? 'highlighted' : ''}`}
        onClick={(e) => this.selectRanked(current)}
      ><hr/></div>
    }
    const rankedCandidatesAndAnchors = rankedCandidates
      .interleave(ranked.map((_, idx) => anchor(idx + 1))).unshift(anchor(0))
    const rankedList = <div className="ranked-candidates">{rankedCandidatesAndAnchors}</div>
    const unrankedList = <div className="unranked-candidates">
      {this.state.unranked.map((candidate, index) =>
        <div key={`unranked-${index}`}>
          <div style={{display: 'flex'}}>
            {Vote.candidateImage(candidate)}
            <Button block bsSize="large" onClick={(e) => this.addToRanked(index)}>
              {candidate.get('name')}
            </Button>
          </div>
        </div>
      )}
    </div>
    return <div>
      <ul>
        <li>Click unranked candidates to add them in order</li>
        <li>Swap 2 ranked candidates by clicking on their names</li>
        <li>Rank as many as you wish</li>
      </ul>
      <Form onSubmit={(e) => e.stopPropagation()}>
        <div className="text-center"><strong>Ranked</strong></div>
        {rankedList}
        <div className="text-center"><strong>Unranked</strong></div>
        {unrankedList}
        {this.state.result === null
          ? <Button disabled={this.state.inSubmission} block bsSize="large" bsStyle="primary"
                    onClick={(e) => this.vote()}>VOTE</Button>
          : <p>Congratulations. You've successfully voted. Your confirmation number is
            <strong> {this.state.result.ballot_id}</strong>. Save this number if you'd like to
            confirm your vote was counted correctly after the election, but keep it private
            or everybody will be able to see how you voted.
          </p>
        }
      </Form>
      <div className="footer-padding">
        {/* A fix for some mobile browsers not being able to click the submit button */}
      </div>
    </div>
  }

  static candidateImage (candidate) {
    const imageUrl = candidate.get('image_url') == null
      ? '../../../static/no_image.png'
      : candidate.get('image_url')
    return <img style={{
      height: 50, width: 'auto', marginRight: 20
    }} src={imageUrl}/>
  }

  async getElectionDetails () {
    try {
      const results = await membershipApi(HTTP_GET, `/election`, {id: this.props.params.electionId})
      this.setState({
        election: Map({name: results.name, number_winners: results.number_winners}),
        unranked: fromJS(_.shuffle(results.candidates))
      })
    } catch (err) {
      return logError('Error loading election', err)
    }
  }

  async vote () {
    if (!confirm('Are you sure you want to submit your vote now? This cannot be undone.')) {
      return Promise()
    }
    if (this.state.inSubmission) {
      return Promise()
    }
    this.setState({inSubmission: true})
    const params = {
      election_id: this.props.params.electionId,
      rankings: this.state.ranked.map(c => c.get('id'))
    }
    try {
      const result = await membershipApi(HTTP_POST, '/vote', params)
      this.setState({result: result})
    } catch (err) {
      return logError('Error submitting vote', err)
    } finally {
      this.setState({inSubmission: false})
    }
  }
}

export default connect((state) => state)(Vote)
