import React, { Component } from 'react'
import { connect } from 'react-redux'
import { membershipApi } from '../../services/membership'
import {
  Button,
  Col,
  FormControl,
  Row
} from 'react-bootstrap'
import {
  HTTP_DELETE,
  HTTP_GET,
  HTTP_POST,
  HTTP_PUT,
  logError
} from '../../util/util'
import { fromJS, List, Map } from 'immutable'

class EmailAdmin extends Component {

  constructor (props) {
    super(props)
    this.state = {
      emails: [],
      inSubmission: false
    }
  }

  componentDidMount () {
    this.getEmails()
  }

  createEmail () {
    this.setState({emails: this.state.emails.push(
      Map({status: 'new', email_address: '', forwarding_addresses: List()}))})
  }

  updateEmailAddress (index, value) {
    let email = this.state.emails.get(index)
    if (email.get('status', null) !== 'new') {
      email = email.set('status', 'modified')
    }
    email = email.set('email_address', value)
    this.setState({emails: this.state.emails.set(index, email)})
  }

  addForwardingAddress (index) {
    let email = this.state.emails.get(index)
    if (email.get('status', null) !== 'new') {
      email = email.set('status', 'modified')
    }
    email = email.set('forwarding_addresses', email.get('forwarding_addresses').push(''))
    this.setState({emails: this.state.emails.set(index, email)})
  }

  deleteForwardingAddress (emailIndex, forwardingIndex) {
    let email = this.state.emails.get(emailIndex)
    if (email.get('status', null) !== 'new') {
      email = email.set('status', 'modified')
    }
    email = email.set('forwarding_addresses', email.get('forwarding_addresses').delete(forwardingIndex))
    this.setState({emails: this.state.emails.set(emailIndex, email)})
  }

  updateForwardingAddress (value, emailIndex, forwardingIndex) {
    let email = this.state.emails.get(emailIndex)
    if (email.get('status', 'unmodified') !== 'new') {
      email = email.set('status', 'modified')
    }
    email = email.setIn(['forwarding_addresses', forwardingIndex], value)
    this.setState({emails: this.state.emails.set(emailIndex, email)})
  }

  render () {
    let admin = false
    const memberData = this.props.member.getIn(['user', 'data'], null)
    if (memberData !== null) {
      memberData.get('roles').forEach((role) => {
        if (role.get('role') === 'admin' && role.get('committee') === 'general') {
          admin = true
        }
      })
    }
    if (!admin) {
      return <div></div>
    }
    const emails = []
    this.state.emails.forEach((email, emailIndex) => {
      let forwardingAddresses = []
      let button = null
      if (!this.state.inSubmission) {
        if (email.get('status', null) === 'new') {
          button = (<Col sm={3}><Button onClick={(e) => this.addEmail(email)}>Add Email</Button></Col>)
        } else if (email.get('status', null) === 'modified') {
          button = (<Col sm={3}><Button onClick={(e) => this.modifyEmail(email)}>Modify Email</Button></Col>)
        }
      }
      email.get('forwarding_addresses').forEach((forwardingAddress, forwardingIndex) => {
        forwardingAddresses.push(
          <div key={`forward-${emailIndex}-${forwardingIndex}`} className="form-inline">
                <FormControl
                  type="text"
                  value={forwardingAddress}
                  onChange={(e) => this.updateForwardingAddress(e.target.value, emailIndex, forwardingIndex)}
                />
                <Button onClick={(e) => this.deleteForwardingAddress(emailIndex, forwardingIndex)}>x</Button>
          </div>
        )
      }
      )
      emails.push(
        <Row key={`email-admin-${emailIndex}`}>
          <Row>
            <Col sm={4}>
              Incoming Email
            </Col>
            <Col sm={4}>
              Forwards to:
            </Col>
          </Row>
          <Row>
            <Col sm={4} className="form-inline">
              <Button onClick={(e) => this.deleteEmail(emailIndex)}>x</Button>
              <FormControl
                type="text"
                value={email.get('email_address')}
                onChange={(e) => this.updateEmailAddress(emailIndex, e.target.value)}
              />
            </Col>
            <Col sm={4}>
              {forwardingAddresses}
              <Button onClick={(e) => this.addForwardingAddress(emailIndex)}>+</Button>
            </Col>
            {button}
          </Row>
        </Row>
      )
    })
    return (
      <div>
        <h2> Emails </h2>
        {emails}
        <Button type="submit" onClick={(e) => this.createEmail()}>Add New Email</Button>
      </div>
    )
  }

  async getEmails () {
    try {
      const results = await membershipApi(HTTP_GET, `/emails`)
      this.setState({emails: fromJS(results)})
    } catch (err) {
      return logError('Error loading /emails', err)
    }
  }

  async addEmail (email) {
    return this.submitEmail(email, HTTP_POST, '/emails')
  }

  async modifyEmail (email) {
    let endpoint = `/emails/${email.get('id')}`
    return this.submitEmail(email, HTTP_PUT, endpoint)
  }

  async deleteEmail (index) {
    let email = this.state.emails.get(index)
    if (email.get('status', null) !== 'new') {
      let confirmed = confirm(`Are you sure you want to delete this email address? Future messages to` +
        ` ${email.get('email_address')} will bounce.`)
      if (!confirmed) {
        return Promise()
      }
      let endpoint = `/emails/${email.get('id')}`
      return this.submitEmail(null, HTTP_DELETE, endpoint)
    } else {
      this.setState({emails: this.state.emails.delete(index)})
    }
  }

  async submitEmail (email, method, endpoint) {
    if (this.state.inSubmission) {
      return Promise()
    }
    this.setState({inSubmission: true})
    try {
      await membershipApi(method, endpoint, email)
      this.getEmails()
    } catch (err) {
      return logError('Error submitting email', err)
    } finally {
      this.setState({inSubmission: false})
    }
  }
}

export default connect((state) => state)(EmailAdmin)
