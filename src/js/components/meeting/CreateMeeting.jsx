import React, { Component } from 'react'
import { membershipApi } from '../../services/membership'
import FieldGroup from '../common/FieldGroup'
import { HTTP_POST, logError } from '../../util/util'
import { Button, Col, Form, Row } from 'react-bootstrap'
import _ from 'lodash'

export default class CreateMeeting extends Component {
  constructor (props) {
    super(props)
    this.state = {
      newMeeting: {
        name: '',
        committee_id: undefined,
        start_time: undefined,
        end_time: undefined
      },
      inSubmission: false
    }
  }

  updateForm (name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }

    let update = this.state[name]
    update[formKey] = value

    this.setState({
      [name]: update
    })
  }

  render () {
    if (!this.props.admin) {
      return <div></div>
    }

    return <div>
      <Row>
        <Col sm={ 4 }>
          <h2>Create Meeting</h2>
          <Form horizontal onSubmit={ (e) => e.preventDefault() }>
            <FieldGroup
              formKey="name"
              type="text"
              label="Meeting"
              value={ this.state.newMeeting.name }
              onFormValueChange={ (formKey, value) => this.updateForm('newMeeting', formKey, value) }
              required
            />
            <Button
              type="submit"
              onClick={ (e) => this.submitForm(e, 'newMeeting', '/meeting') }>
              Create Meeting
            </Button>
          </Form>
        </Col>
      </Row>
    </div>
  }

  async submitForm (e, name, endpoint) {
    e.preventDefault()
    if (this.state.newMeeting.name === '') {
      logError('Please provide a meeting name.')
      return
    }

    if (this.state.inSubmission) {
      return
    }
    this.setState({
      inSubmission: true
    })

    try {
      await membershipApi(HTTP_POST, endpoint, this.state[name])
      if (_.isFunction(this.props.onMeetingCreated)) {
        this.props.onMeetingCreated()
      }
    } catch (err) {
      return logError('Error creating meeting', err)
    } finally {
      this.setState({inSubmission: false})
    }
  }

}
