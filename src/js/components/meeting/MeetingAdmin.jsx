import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Button,
  Col,
  ControlLabel,
  Form,
  FormControl,
  FormGroup
} from 'react-bootstrap'
import Autosuggest from 'react-autosuggest'
import { Map } from 'immutable'
import { Meetings } from '../../client/MeetingClient'
import { Members } from '../../client/MemberClient'

const getSuggestionValue = suggestion => suggestion.name

class MeetingAdmin extends Component {

  constructor (props) {
    super(props)
    this.suggestionsCache = {}
    this.state = {
      meeting: Map({id: 0, name: 'Loading...'}),
      attendees: null,
      suggestions: [],
      searchInput: '',
      inSubmission: false
    }
  }

  componentDidMount () {
    this.getMeeting()
    this.getMeetingAttendees()
  }

  render () {
    const agenda = this.state.meeting.get('landing_url')
      ? <a href={this.state.meeting.get('landing_url')}>{this.state.meeting.get('landing_url')}</a> : ''

    const attendees = this.state.attendees

    const attendeesView = attendees ? (attendees.size ? attendees.map((attendee, index) => {
      return (
        <div key={`attendee-${index}`}>
          {`${attendee.get('name')} `}
          <a href='javascript:void(0)' className='attendee-remove' onClick={(e) => this.removeAttendeeClicked(attendee)}>X</a>
        </div>
      )
    }) : 'None') : 'Loading...'

    const inputProps = {
      placeholder: 'Name or email address',
      className: 'form-control member-search',
      value: this.state.searchInput,
      onChange: (e, args) => this.onChange(e, args)
    }

    const renderMember = (member, { query, isHighlighted }) => (
      <div className={isHighlighted ? 'suggestion-highlighted' : ''}>
      <a href='javascript:void(0)'>{member.name}</a>
      </div>
    )

    return (
      <div className='meeting-admin'>
        <h2>
          {this.state.meeting.get('name')} <small>
          {this.state.meeting.get('code') ? '#' + this.state.meeting.get('code') : ''}</small>
        </h2>
        {agenda}
        <h3>Edit Meeting</h3>
          <Form horizontal onSubmit={ (e) => e.updateLandingUrl() }>
            <FormGroup controlId="formLandingUrl">
              <Col componentClass={ControlLabel} sm={2}>
                Landing URL
              </Col>
              <Col sm={6}>
                <FormControl type="text" onChange={(e) => this.updateLandingUrl(e.target.value)}/>
              </Col>
            </FormGroup>
            <Button
              type="submit"
              onClick={ (e) => this.submitLandingUrl(e) }>
              Submit
            </Button>
          </Form>
        <h3>Sign In</h3>
        <Autosuggest
          suggestions={this.state.suggestions}
          onSuggestionsFetchRequested={(e) => this.onSuggestionsFetchRequested(e)}
          onSuggestionsClearRequested={(e) => this.onSuggestionsClearRequested()}
          onSuggestionSelected={(e, args) => this.onSuggestionSelected(e, args)}
          highlightFirstSuggestion={true}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderMember}
          inputProps={inputProps}
        />
        <h3>{'Attendees ' + (attendees ? ('(' + attendees.size + ')') : '')}</h3>
        {attendeesView}
      </div>
    )
  }

  updateLandingUrl (landingUrl) {
    if (this.state.inSubmission) {
      return
    }

    this.setState({landingUrl: landingUrl})
  }

  async submitLandingUrl (e) {
    e.preventDefault()

    if (this.state.inSubmission) {
      return
    }
    this.setState({
      inSubmission: true
    })

    try {
      const result = await Meetings.setLandingUrl(this.state.meeting.get('id'), this.state.landingUrl)
      const updatedMeeting = this.state.meeting.merge(result.get('meeting'))
      this.setState({meeting: updatedMeeting})
    } finally {
      this.setState({inSubmission: false})
    }
  }

  onChange (event, args) {
    this.setState({
      searchInput: args.newValue
    })
  }

  async onSuggestionSelected (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) {
    const member = suggestion
    await Meetings.addAttendee(this.props.params.meetingId, member.id)
    this.setState({searchInput: ''})
    this.getMeetingAttendees()
  }

  async onSuggestionsFetchRequested (event) {
    const inputValue = event.value.trim()
    const inputLength = inputValue.length

    if (inputLength === 0) {
      this.onSuggestionsClearRequested()
    } else {
      if (!this.suggestionsCache[inputValue]) {
        const results = await Members.search(inputValue)
        const members = results.toJS().members
        this.suggestionsCache[inputValue] = members
      }
      const suggestions = this.suggestionsCache[inputValue]
      this.setState({
        suggestions: suggestions
      })
    }
  }

  onSuggestionsClearRequested () {
    this.setState({
      suggestions: []
    })
  }

  async getMeetingAttendees () {
    const meetingId = this.props.params.meetingId
    const results = await Meetings.getAttendees(meetingId)
    this.setState({attendees: results})
  }

  async removeAttendeeClicked (member) {
    const meetingName = this.state.meeting.get('name')
    if (confirm(`Are you sure you want to remove ${member.get('name')} from the list of attendees for ${meetingName}?`)) {
      await Meetings.removeAttendee(this.props.params.meetingId, member.get('id'))
      this.getMeetingAttendees()
    }
  }

  async getMeeting () {
    const meeting = await Meetings.get(this.props.params.meetingId)
    this.setState({meeting: meeting})
  }
}

export default connect((state) => state)(MeetingAdmin)
