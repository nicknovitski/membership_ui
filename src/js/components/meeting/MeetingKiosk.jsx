import React, { Component } from 'react'
import { connect } from 'react-redux'
import { membershipApi } from '../../services/membership'
import FieldGroup from '../common/FieldGroup'
import {
  HTTP_GET,
  HTTP_POST,
  logError
} from '../../util/util'
import {
  Button,
  Col,
  Form
} from 'react-bootstrap'
import { Fn } from '../../functional'
import { Map } from 'immutable'
import { serviceLocator } from '../../util/serviceLocator'

const ValidEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
function isValidEmail (email) {
  return ValidEmail.test(email)
}

const ValidName = /^[a-zA-Z]/
function isValidName (name) {
  return ValidName.test(name)
}

function validationStateFor (nullableBoolean) {
  switch (nullableBoolean) {
    case null: return null
    case true: return 'success'
    case false: return 'error'
    default: return null
  }
}

class MeetingKiosk extends Component {

  constructor (props) {
    super(props)
    this.state = {
      meeting: Map({id: 0, name: ''}),
      attendee: {first_name: '', last_name: '', email_address: ''},
      inSubmission: false
    }
  }

  componentDidMount () {
    this.getMeeting()
  }

  updateForm (name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    let update = this.state[name]
    update[formKey] = value
    this.setState({[name]: update})
  }

  static attendeeValidationStates (attendee) {
    const emailValidState = validationStateFor(Fn.falsyOr(isValidEmail, attendee.email_address))
    const firstNameValidState = validationStateFor(Fn.falsyOr(isValidName, attendee.first_name))
    const lastNameValidState = validationStateFor(Fn.falsyOr(isValidName, attendee.last_name))
    const hasFieldError = [emailValidState, firstNameValidState, lastNameValidState].includes('error')
    return {
      email_address: emailValidState,
      first_name: firstNameValidState,
      last_name: lastNameValidState,
      invalid: hasFieldError
    }
  }

  render () {
    const validation = MeetingKiosk.attendeeValidationStates(this.state.attendee)
    return (
      <div>
        <h2> Welcome to {this.state.meeting.get('name')} </h2>
        <div>
          <Col sm={4}>
            <h2>Check in</h2>
            <Form horizontal onSubmit={(e) => e.preventDefault()}>
              <FieldGroup
                autoFocus={true}
                formKey="email_address"
                type="email"
                label="Email Address"
                value={this.state.attendee.email_address}
                validationState={validation.email_address}
                onFormValueChange={(formKey, value) => this.updateForm('attendee', formKey, value)}
                required
              />
              <FieldGroup
                formKey="first_name"
                type="text"
                label="First name"
                value={this.state.attendee.first_name}
                validationState={validation.first_name}
                onFormValueChange={(formKey, value) => this.updateForm('attendee', formKey, value)}
                required
              />
              <FieldGroup
                formKey="last_name"
                type="text"
                label="Last Name"
                value={this.state.attendee.last_name}
                validationState={validation.last_name}
                onFormValueChange={(formKey, value) => this.updateForm('attendee', formKey, value)}
                required
              />
              <Button type="submit"
                      disabled={validation.invalid}
                      onClick={(e) => this.submitForm(e, 'attendee', `/meetings/${this.props.params.meetingId}/attendee`)}>
                Check In
              </Button>
            </Form>
          </Col>
        </div>
      </div>
    )
  }

  async getMeeting () {
    try {
      const results = await membershipApi(HTTP_GET, `/meetings/${this.props.params.meetingId}`)
      this.setState({meeting: Map(results)})
    } catch (err) {
      return logError(`Error loading meeting ${this.props.params.meetingId}`, err)
    }
  }

  async submitForm (e, key, endpoint) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return Promise()
    }
    this.setState({inSubmission: true})
    try {
      await membershipApi(HTTP_POST, endpoint, this.state[key])
      this.setState({attendee: {first_name: '', last_name: '', email_address: ''}})
      serviceLocator.notificationSystem.addNotification({
        title: 'Welcome to the meeting!',
        level: 'success'
      })
    } catch (err) {
      return logError(`Error checking in to ${this.props.params.meetingId}`, err)
    } finally {
      this.setState({inSubmission: false})
    }
  }
}

export default connect((state) => state)(MeetingKiosk)
