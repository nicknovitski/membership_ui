import React, { Component } from 'react'
import {
  Button,
  Col,
  Glyphicon,
  Row
} from 'react-bootstrap'
import { Map, Set } from 'immutable'
import { Link } from 'react-router'
import { Meetings } from '../../client/MeetingClient'

export default class MeetingList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      editingMeetingId: null,
      meetings: new Map(),
      inSubmission: false
    }
  }

  componentWillMount () {
    this.updateMeetings()
  }

  updateMeetings () {
    Meetings.all().then((meetings) => {
      const meetingsWithCodeClaimed = new Map(meetings.map((m) => {
        const code = m.get('code') || ''
        return [m.get('id'), m.set('code', code).set('codeSaved', code).set('isValid', true)]
      }))
      this.setState({meetings: meetingsWithCodeClaimed})
    })
  }

  // pass null to generate a random code
  claimMeetingCode (meeting, code) {
    const meetingId = meeting.get('id')
    const result = code === null
      ? Meetings.generateMeetingCode(meetingId)
      : Meetings.setMeetingCode(meetingId, code)
    result.then((rsp) => {
      const meetingResponse = rsp.get('meeting')
      const codeSaved = rsp.get('code')
      const updatedMeeting = meeting.merge(meetingResponse).set('codeSaved', codeSaved)
      this.setState({
        meetings: this.state.meetings.set(meetingId, updatedMeeting)
      })
    })
  }

  unclaimMeetingCode (meeting) {
    const meetingId = meeting.get('id')
    Meetings.setMeetingCode(meetingId, '').then((rsp) => {
      const updatedMeeting = meeting.set('codeSaved', '')
      this.setState({
        meetings: this.state.meetings.set(meetingId, updatedMeeting)
      })
    })
  }

  onFocusMeeting (meeting) {
    this.setState({
      editingMeetingId: meeting.get('id')
    })
  }

  onChangeMeetingCode (meeting, code) {
    const meetingId = meeting.get('id')
    const isValid = code.length === 4 ? Meetings.isValidMeetingCode(code) : false
    const updatedMeeting = meeting
      .set('code', code)
      .set('isValid', isValid)
    this.setState({
      meetings: this.state.meetings.set(meetingId, updatedMeeting)
    })
  }

  actionForMeetingCode (meeting) {
    const code = meeting.get('code')
    const codeSaved = meeting.get('codeSaved')
    const isDirty = code !== codeSaved
    const isValid = meeting.get('isValid')
    if (codeSaved && !isDirty) {
      return 'Unset'
    } else if (code === '' || isValid) {
      return 'Set'
    } else {
      return 'Cancel'
    }
  }

  resetMeetingCode (meeting) {
    const meetingId = meeting.get('id')
    const unchangedCode = meeting.get('codeSaved')
    const resetMeeting = meeting
      .set('code', unchangedCode)
      .set('isValid', true)
    this.setState({
      meetings: this.state.meetings.set(meetingId, resetMeeting)
    })
  }

  onSubmitMeetingCode (meeting, action) {
    const newCode = meeting.get('code')
    switch (action) {
      case 'Set':
        if (newCode === '') {
          // generate a new code
          this.claimMeetingCode(meeting, null)
        } else {
          // set the given code
          this.claimMeetingCode(meeting, newCode)
        }
        break
      case 'Unset':
        // set to valid code if it has changed
        this.unclaimMeetingCode(meeting)
        break
      case 'Cancel':
        // reset text box
        this.resetMeetingCode(meeting)
        break
      default:
        throw new Error(`Unrecognized meeting code action: ${action}`)
    }
  }

  render () {
    const meetingRows = this.state.meetings.valueSeq().map((meeting) => {
      const code = meeting.get('code')
      const codeSaved = meeting.get('codeSaved')
      const meetingId = meeting.get('id')
      const isDirty = codeSaved !== code
      const isSelected = this.state.editingMeetingId === meetingId
      const landingUrl = meeting.get('landing_url')
      let actions = new Set()
      if (isSelected) {
        const editAction = this.actionForMeetingCode(meeting)
        actions = actions.add(editAction)
      }
      if (isDirty) {
        actions = actions.add('Cancel')
      }
      const actionButtons = actions.toSeq().map((action) =>
        <Button
          onFocus={(e) => this.onFocusMeeting(meeting)}
          onClick={(e) => this.onSubmitMeetingCode(meeting, action)}>
          {action === 'Cancel' && codeSaved === '' ? 'Clear' : action}
        </Button>
      )
      const classNames = []
      if (isSelected) classNames.push('selected')
      if (isDirty) classNames.push('dirty')
      return <Row key={`meeting${meetingId}`}
                  className={classNames.join(' ')}>
        <Col sm={3}>
          <h4>
            {meeting.get('name', '[ERROR]') + ' '}
            {landingUrl && <Button href={landingUrl} bsSize="small"><Glyphicon glyph="link"/></Button>}
          </h4>
        </Col>
        { this.props.admin &&
        <Col sm={6}>
          <Link to={`/meetings/${meetingId}/admin`}>Admin</Link>
          &nbsp;&nbsp;
          <Link to={`/meetings/${meetingId}/kiosk`}>Kiosk</Link>
          &nbsp;&nbsp;
          Meeting code: <label>
          <input type="text"
                 value={code}
                 maxLength={4}
                 onChange={(e) => {
                   const meetingCode = e.target.value
                   this.onChangeMeetingCode(meeting, meetingCode)
                 }}
                 onKeyDown={(e) => {
                   if (e.keyCode === 13) {
                     e.preventDefault()
                     e.stopPropagation()
                     this.onSubmitMeetingCode(meeting)
                   }
                 }}
                 onFocus={(e) => {
                   this.onFocusMeeting(meeting)
                   e.target.select()
                 }}
          />
          </label>
          {actionButtons}
        </Col>
        }
      </Row>
    })
    return <div>{meetingRows}</div>
  }

}
