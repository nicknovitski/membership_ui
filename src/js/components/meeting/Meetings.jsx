import React, { Component } from 'react'
import { connect } from 'react-redux'
import { membershipApi } from '../../services/membership'
import { HTTP_GET, logError } from '../../util/util'
import { Col, Row } from 'react-bootstrap'
import { List } from 'immutable'
import { Link } from 'react-router'
import CreateMeeting from './CreateMeeting'
import MeetingList from './MeetingList'

class Meetings extends Component {

  constructor (props) {
    super(props)
    this.state = {
      meetings: [],
      inSubmission: false
    }
  }

  componentDidMount () {
    this.getMeetings()
  }

  render () {
    let admin = false
    const memberData = this.props.member.getIn(['user', 'data'], null)
    if (memberData !== null) {
      memberData.get('roles').forEach((role) => {
        if (role.get('role') === 'admin' && role.get('committee') === 'general') {
          admin = true
        }
      })
    }

    const meetings = []
    this.state.meetings.forEach((meeting) => {
      meetings.push(
        <Row key={`meeting-toolbox-${meeting.id}`} className="meeting-toolbox">
          <Col sm={3}>
          <Link to={`/meetings/${meeting.id}/`}>{meeting.name}</Link>
          </Col>
        </Row>
      )
    })
    return (
      <div>
        <h2> Meetings </h2>
        <MeetingList admin={admin} ref={(meetingList) => { this.meetingList = meetingList }} />
        <CreateMeeting admin={admin} onMeetingCreated={() => this.meetingList.updateMeetings()} />
      </div>
    )
  }

  async getMeetings () {
    try {
      const results = await membershipApi(HTTP_GET, `/meeting/list`)
      this.setState({meetings: List(results)})
    } catch (err) {
      return logError('Error loading /meeting/list', err)
    }
  }
}

export default connect((state) => state)(Meetings)
