import React, {Component} from 'react'
import {connect} from 'react-redux'
import {membershipApi} from '../../services/membership'
import {HTTP_GET, HTTP_POST, logError} from '../../util/util'
import {fromJS, List, Map} from 'immutable'
import {Link} from 'react-router'
import AddMeeting from '../admin/AddMeeting'
import AddRole from '../admin/AddRole'
import {Button, Form} from 'react-bootstrap'
import dateFormat from 'dateformat'
import {Fn} from '../../functional'

dateFormat.masks.dsa = 'dddd, mmmm dS \'at\' h:MM TT'

class MemberPageComponent extends Component {

  constructor (props) {
    super(props)
    this.state = {
      member: null,
      elections: new Map(),
      inSubmission: false,
      meetingShortId: ''
    }
  }

  componentDidMount () {
    this.fetchMemberData()
  }

  componentDidUpdate (prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.fetchMemberData()
    }
  }

  render () {
    const roles = []
    const meetings = []
    const memberData = this.state.member
    if (memberData === null) {
      return null
    }
    memberData.get('roles').forEach((role, index) => {
      roles.push(
        <div key={`role-${index}`}>{`${role.get('role')}: ${role.get('committee')}`}</div>
      )
    })
    memberData.get('meetings').forEach((meeting, index) => {
      meetings.push(
        <div key={`meeting-${index}`}>{meeting}</div>
      )
    })
    const admin = this.isAdmin()
    return (
      <div>
        <h2>Member Info</h2>
        <div>{`${memberData.getIn(['info', 'first_name'])} ${memberData.getIn(['info', 'last_name'])}`}</div>
        <h2>Committees</h2>
        {roles}
        <h2>Meetings attended</h2>
        <Form inline onSubmit={(e) => { this.attendMeeting(e) }}>
          <label htmlFor="meetingCode">Meeting Code: #</label>
          {' '}
          <input
            id="meetingCode"
            type="text"
            placeholder="0000"
            maxLength="4"
            value={Fn.falsyOr((id) => `${id}`, this.state.meetingShortId)}
            onChange={(e) => {
              const meetingCode = e.target.value
              this.setState({meetingShortId: meetingCode})
            }}
          />
          <Button type="submit">Join</Button>
        </Form>
        {meetings}
        <h2>Eligible Votes</h2>
        {this.renderEligibleVotes(memberData.get('votes'))}
        {admin &&
        <div>
          <AddMeeting
            admin={admin}
            memberId={this.props.params.memberId ? this.props.params.memberId : this.state.member.get('id')}
            refresh={() => this.fetchMemberData()}
          />
          <AddRole
            admin={admin}
            memberId={this.props.params.memberId ? this.props.params.memberId : this.state.member.get('id')}
            refresh={() => this.fetchMemberData()}
          />
        </div>
        }
      </div>
    )
  }

  renderEligibleVotes (votes) {
    if (!this.state.elections) {
      return new List()
    }
    return votes.flatMap((vote, index) => {
      const electionId = vote.get('election_id')
      if (!this.state.elections.has(electionId)) {
        return new List()
      }
      const election = this.state.elections.get(electionId)
      return new List([<div key={`vote-${index}`}>
        {this.renderElectionDescription(electionId, election, vote.get('voted'))}
      </div>])
    })
  }

  renderElectionDescription (electionId, election, hasVoted) {
    const electionPageLink = <Link to={`/elections/${electionId}/`}>
      <strong>{election.get('name')}</strong>
    </Link>
    const electionName = election.get('name', '[UNNAMED ELECTION]')
    const votingPageLink = <Link to={`/vote/${electionId}/`}>
      <strong>{electionName}</strong>
    </Link>
    const now = new Date().getTime()
    if (election.get('status') === 'published') {
      const beginning = election.get('voting_begins_epoch_millis')
      const ending = election.get('voting_ends_epoch_millis')
      if (beginning && beginning > now) {
        return <span>{electionPageLink}: Voting has not yet begun. Polls will open on {
          dateFormat(new Date(beginning), 'dsa')}</span>
      } else if (ending && ending < now) {
        return <span>{electionPageLink}: Voting has closed.</span>
      } else if (ending) {
        return <span>{hasVoted ? electionPageLink : votingPageLink}: Voting is now open! Polls will close on {
          dateFormat(new Date(ending), 'dsa')}</span>
      } else {
        return <span>
          {hasVoted
          ? `${electionName}: Your ballot has been submitted`
          : [votingPageLink, ': Voting is now open!']
        }</span>
      }
    }
    if (election.get('status') === 'final') {
      return <span>{electionPageLink}: The election is closed.</span>
    }
    if (election.get('status') === 'canceled') {
      return <span>{electionPageLink}: The election was canceled.</span>
    }
  }

  async fetchMemberData () {
    let member
    if (this.props.params.memberId) {
      try {
        const results = await membershipApi(HTTP_GET, `/admin/member/details`, {member_id: this.props.params.memberId})
        member = fromJS(results)
      } catch (err) {
        return logError('Error loading member details', err)
      }
    } else {
      try {
        const results = await membershipApi(HTTP_GET, `/member/details`)
        member = fromJS(results)
      } catch (err) {
        return logError('Error loading member details', err)
      }
    }
    this.setState({member: member})
    member.get('votes').forEach((vote) => this.fetchElection(vote.get('election_id')))
  }

  async fetchElection (electionId) {
    try {
      const results = await membershipApi(HTTP_GET, `/election`, {id: electionId})
      const newState = new Map(this.state.elections)
      newState.set(electionId, fromJS(results))
      this.setState({elections: newState})
    } catch (err) {
      return logError('Error loading election details', err)
    }
  }

  isAdmin () {
    const memberData = this.props.member.getIn(['user', 'data'], null)
    let admin = false
    if (memberData !== null) {
      memberData.get('roles').forEach((role) => {
        if (role.get('role') === 'admin' && role.get('committee') === 'general') {
          admin = true
        }
      })
    }
    return admin
  }

  async attendMeeting (e) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({inSubmission: true})
    try {
      const meeting = await membershipApi(HTTP_POST, '/meeting/attend', {'meeting_short_id': this.state.meetingShortId})
      this.setState({meetingShortId: ''})
      const landingUrl = fromJS(meeting).get('landing_url', null)
      if (landingUrl) {
        // TODO: Figure out how to inject this for testing
        location.href = landingUrl
      }
      this.fetchMemberData()
    } catch (err) {
      return logError('Error adding attendee', err)
    } finally {
      this.setState({inSubmission: false})
    }
  }

}

export default connect((state) => state)(MemberPageComponent)
