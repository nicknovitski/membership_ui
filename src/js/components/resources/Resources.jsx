import React, { Component } from 'react'
import { connect } from 'react-redux'

class Resources extends Component {
  render () {
    return (
      <div>
        <h2>DSA San Francisco Member Resources</h2>

        <h3>Chapter Documents</h3>

        <ul>
          <li><a href="https://dsasf.org/bylaws">Bylaws</a></li>
          <li><a href="https://dsasf.org/code_of_conduct/">Code of Conduct</a></li>
          <li><a href="https://bit.ly/dsasf-handbook">Handbook</a></li>
        </ul>

        <h3>Internal Communications</h3>

        <ul>
          <li><a href="https://bit.ly/dsasf-calendar">Calendar</a></li>
          <li><a href="https://dsasfteam.slack.com">Slack</a></li>
          <li><a href="https://groups.google.com/forum/#!forum/dsasf-members">Google Group</a></li>
          <li><a href="https://discourse.dsasf.org">Discourse</a></li>
          <li><a href="https://dsasf.consider.it/">Consider.it</a></li>
          <li><a href="https://www.youtube.com/channel/UCcnao-2NMyRbTTvUAYx0Y2A">Youtube</a></li>
        </ul>

        <h3>Service Committee Resources</h3>

        <h4>Conflict Resolution</h4>

        <ul>
          <li><a href="https://dsasf.org/request-conflict-resolution/">Request conflict resolution</a></li>
        </ul>

        <h4>Creative</h4>

        <ul>
          <li><a href="https://bit.ly/dsa-request">Request design work</a></li>
        </ul>

        <h4>Outreach</h4>

        <ul>
          <li>Email <a href="mailto:dsa-sf-outreach@googlegroups.com">dsa-sf-outreach@googlegroups.com</a> to add an event to the public calendar</li>
        </ul>

        <h4>Tech</h4>

        <ul>
          <li>Email <a href="mailto:eligibility@dsasf.org">eligibility@dsasf.org</a> for election elibility support, or to request access to the Google Group, Facebook Group, and other chapter communications channels.</li>
          <li>Email <a href="mailto:wordpress@dsasf.org">wordpress@dsasf.org</a> for help with <a href="https://dsasf.org">dsasf.org</a>, our chapter website.</li>
          <li>Email <a href="mailto:tech-support@dsasf.org">tech-support@dsasf.org</a> for general tech help.</li>
          <li>Email <a href="mailto:livemeeting@dsasf.org">livemeeting@dsasf.org</a> to get access to video of last month's general chapter meeting.</li>
        </ul>

      </div>

    )
  }
}

export default connect((state) => state)(Resources)
