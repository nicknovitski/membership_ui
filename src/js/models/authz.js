import {ImmutableModel} from '.'
import {List, Set} from 'immutable'

export function AzRole (name, committeeId = null) {
  if (this) {
    ImmutableModel.call(this, List([name, committeeId]))
    this.name = name
    this.committeeId = committeeId
    return this
  } else {
    return new AzRole(name, committeeId)
  }
}
AzRole.prototype = Object.create(ImmutableModel.prototype)

export function Authorization (memberId, roles) {
  if (!memberId) {
    throw new Error('Authorization.memberId required')
  }
  if (this) {
    const roleSet = Set(roles || [])
    ImmutableModel.call(this, List([memberId, roleSet]))
    this.memberId = memberId
    this.roles = roleSet
    return this
  } else {
    return new Authorization(memberId, roles)
  }
}

Authorization.prototype = Object.create(ImmutableModel.prototype)
Authorization.prototype.hasRole = function (roleName, scope = {}) {
  if (typeof scope !== 'object') {
    throw Error('second argument to hasRole (scope) must be an object')
  }
  const committeeId = scope['committeeId'] || null
  const memberId = scope['memberId'] || null
  return this.memberId === memberId || this.roles.has(AzRole(roleName, committeeId))
}
