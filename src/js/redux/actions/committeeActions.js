import { fromJS } from 'immutable'
import * as types from '../constants/actionTypes'
import { membershipApi } from '../../services/membership'
import {
  HTTP_GET,
  HTTP_POST
} from '../../util/util'

// thunk that requests data and handles success and error cases
export const addCommittee = ({ adminEmails, member, name }) => {
  return async dispatch => {
    try {
      const committee = await membershipApi(
        HTTP_POST,
        `/committee`,
        {
          admin_list: adminEmails,
          name: name
        }
      )
      dispatch(addCommitteeSucceeded(fromJS(committee)))
    } catch (err) {
      dispatch(addCommitteeFailed(err))
    }
  }
}

export const addCommitteeFailed = (error) => (
  {
    type: types.ADD_COMMITTEE_FAILED,
    payload: { error }
  }
)

export const addCommitteeSucceeded = (committee) => {
  // TODO: return committee and ID from API
  const tempCommittee = fromJS({
    99: 'updated'
  })
  return {
    type: types.ADD_COMMITTEE_SUCCEEDED,
    payload: tempCommittee
  }
}

export const fetchCommittees = member => {
  return async dispatch => {
    try {
      const committees = await membershipApi(HTTP_GET, `/committee/list`)
      dispatch(requestCommitteesSucceeded(member, fromJS(committees)))
    } catch (err) {
      dispatch(requestCommitteesFailed(err))
    }
  }
}

export const requestCommittees = member => (
  {
    type: types.REQUEST_COMMITTEES,
    payload: { member }
  }
)

export const requestCommitteesFailed = (error) => (
  {
    type: types.REQUEST_COMMITTEES_FAILED,
    payload: { error }
  }
)

export const requestCommitteesSucceeded = (member, committees) => (
  {
    type: types.REQUEST_COMMITTEES_SUCCEEDED,
    payload: { committees }
  }
)
