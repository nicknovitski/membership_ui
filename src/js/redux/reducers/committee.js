import { fromJS, Map } from 'immutable'
import * as types from '../constants/actionTypes'

const INITIAL_STATE = fromJS({
  committees: {}
})

function committee (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.ADD_COMMITTEE:
      return state
    case types.ADD_COMMITTEE_SUCCEEDED:
      return state.mergeDeep(Map({
        committees: action.payload
      }))
    case types.FETCH_COMMITTEES:
      return state
    case types.REQUEST_COMMITTEES_SUCCEEDED:
      return state.mergeDeep(action.payload)
    default:
      return state
  }
}

export default committee
