import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import auth from './auth'
import committee from './committee'
import member from './member'

const rootReducer = combineReducers({
  auth,
  member,
  committee,
  routing: routerReducer
})

export default rootReducer
