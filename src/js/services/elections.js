export function eligibleVoteStatus (election, vote) {
  if (!vote) {
    return 'ineligible'
  }
  const electionId = election.get('id')
  if (electionId && vote.get('election_id').toString() === electionId) {
    if (Object.is(vote.get('voted'), true)) {
      return 'already voted'
    } else {
      return 'eligible'
    }
  } else {
    return 'ineligible'
  }
}

export function electionStatus (election, now = new Date()) {
  const electionState = election.get('status')
  if (electionState !== 'published') {
    return electionState
  }
  const beginning = election.get('voting_begins_epoch_millis')
  const ending = election.get('voting_ends_epoch_millis')
  if (beginning && beginning > now) {
    return 'polls not open'
  } else if (ending && ending < now) {
    return 'polls closed'
  }
  return 'polls open'
}
