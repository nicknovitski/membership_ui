import {List} from 'immutable'

export function isAdmin (member) {
  return member.getIn(['user', 'data', 'roles'], List())
    .some((role) => role.get('role') === 'admin' && role.get('committee') === 'general')
}

const members = {
  isAdmin
}

export default members
