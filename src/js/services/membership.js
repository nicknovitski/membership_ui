import { MEMBERSHIP_API_URL } from '../config'
import api from './api'

/**
 * @deprecated Use {@link ApiClient} instead
 */
export function membershipApi (method, route, params = {}) {
  return api(method, `${MEMBERSHIP_API_URL}${route}`, params)
}
