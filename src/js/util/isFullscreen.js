import { stringOf } from './getOfType'

export const isFullscreen = path =>
  stringOf(path)
    .split('/')
    .pop() === 'fullscreen'
