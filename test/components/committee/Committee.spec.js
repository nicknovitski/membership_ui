import React from 'react'
import configureStore from '../../../src/js/redux/store/configureStore'
import { serviceLocator } from '../../../src/js/util/serviceLocator'

import Committees from '../../../src/js/components/committee/Committees'
import { expect } from 'chai'
import { shallow } from 'enzyme'

const store = configureStore()
serviceLocator.store = store

describe('<Committees />', () => {
  it('works', (done) => {
    const wrapper = shallow(<Committees store={store} />).dive()

    expect(wrapper.find('h2')).to.have.length(2)
    done()
  })
})
