import { expect } from 'chai'
import { fromJS } from 'immutable'
import committee from '../../../src/js/redux/reducers/committee'
import * as types from '../../../src/js/redux/constants/actionTypes'

describe('committee reducers', () => {
  describe('ADD_COMMITTEE_SUCCEEDED', () => {
    it('adds the new committee to the committee list', () => {
      const state = fromJS({
        committees: {
          1: 'test-1'
        },
        other: 'value'
      })
      const action = {
        type: types.ADD_COMMITTEE_SUCCEEDED,
        payload: fromJS({ 2: 'test-2' })
      }
      const expectedState = fromJS({
        committees: {
          1: 'test-1',
          2: 'test-2'
        },
        other: 'value'
      })

      const result = committee(state, action)

      expect(result).to.deep.equal(expectedState)
    })
  })
})
