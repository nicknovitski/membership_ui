import {expect} from 'chai'
import {electionStatus, eligibleVoteStatus} from '../../src/js/services/elections'
import {Map} from 'immutable'

function fixtureEligiblePollsOpenAndPublished ({
  id = new Date().getTime(),
  when = new Date()
} = {}) {
  const anHourAgo = new Date(when.setHours(when.getHours() - 1))
  const nextHour = new Date(when.setHours(when.getHours() + 1))
  return {
    election: Map({
      id,
      status: 'published',
      voting_begins_epoch_millis: anHourAgo.getTime(),
      voting_ends_epoch_millis: nextHour.getTime()
    }),
    vote: Map({
      election_id: id,
      voted: false
    }),
    when
  }
}

const ONE_DAY = 24 * 60 * 60 * 1000
const now = new Date()
const yesterday = new Date(now.getTime() - ONE_DAY)
const tomorrow = new Date(now.getTime() + ONE_DAY)

describe('elections.electionStatus', () => {
  it('returns "polls open" when the election is published and starts before now and ends after now', () => {
    const { election } = fixtureEligiblePollsOpenAndPublished()
    const result = electionStatus(election, {when: now})
    expect(result).to.equal('polls open')
  })

  it('returns "polls open" when the election is published and has no start or end date', () => {
    const result = electionStatus(Map({status: 'published'}), now)
    expect(result).to.equal('polls open')
  })

  it('returns "polls open" when the election is published and has no start date and ends in the future', () => {
    const { election } = fixtureEligiblePollsOpenAndPublished({when: now})
    const result = electionStatus(election.remove('voting_ends_epoch_millis'), now)
    expect(result).to.equal('polls open')
  })

  it('returns "not started" when the election is published and ends in the past', () => {
    const { election } = fixtureEligiblePollsOpenAndPublished({when: yesterday})
    const result = electionStatus(election, now)
    expect(result).to.equal('closed')
  })

  it('returns "polls closed" when the election is published and ends in the past', () => {
    const { election } = fixtureEligiblePollsOpenAndPublished({when: tomorrow})
    const result = electionStatus(election, now)
    expect(result).to.equal('not started')
  })
})

describe('elections.eligibleVoteStatus', () => {
  it('returns "eligible" when the election status is "polls open" and the eligible voter has NOT voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = eligibleVoteStatus(election, vote)
    expect(result).to.equal('eligible')
  })

  it('returns "eligible" when the election status is "draft" and the eligible voter has NOT voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = eligibleVoteStatus(election.set('status', 'draft'), vote)
    expect(result).to.equal('eligible')
  })

  it('returns "eligible" when the election status is "final" and the eligible voter has NOT voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = eligibleVoteStatus(election.set('status', 'draft'), vote)
    expect(result).to.equal('eligible')
  })

  it('returns "already voted" when the election status is "polls open" and the eligible voter has voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = eligibleVoteStatus(election, vote.set('voted', true))
    expect(result).to.equal('already voted')
  })

  it('returns "already voted" when the election status is "draft" and the eligible voter has voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = eligibleVoteStatus(election.set('status', 'draft'), vote.set('voted', true))
    expect(result).to.equal('already voted')
  })

  it('returns "already voted" when the election status is "final" and the eligible voter has voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = eligibleVoteStatus(election.set('status', 'final'), vote.set('voted', true))
    expect(result).to.equal('already voted')
  })

  it('returns "ineligible" when the vote has a different election.id', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = eligibleVoteStatus(election, vote.set('election_id', election.get('id') - 1))
    expect(result).to.equal('ineligible')
  })

  it('returns "ineligible" when the vote is null', () => {
    const { election } = fixtureEligiblePollsOpenAndPublished()
    const result = eligibleVoteStatus(election, null)
    expect(result).to.equal('ineligible')
  })
})
